from peewee import *
from database import *
import datetime

today = datetime.date.today()
week = datetime.timedelta(days=-7)
lastweek = today + week
weekdate = lastweek.strftime("%Y-%m-%d")

f = open(weekdate + '.html','w')
print weekdate




query = News.raw('select date, id, cat, title, url, body from news where date > \'' + weekdate + '\' order by cat')
category = "";
for news in query:
  if (category != news.cat):
    f.write('\n<h2>' + news.cat + '</h2>\n')
    category = news.cat

  try:
    f.write('<a href=' + news.url + '>' + news.title.encode('utf-8') + '</a><br>\n')
  except UnicodeDecodeError:
    f.write('<a href=' + news.url + '>' + news.title.replace(u"\u2018", "'").replace(u"\u2014", "-").replace(u"\u2019", "'").replace(u"\u2013", "-").replace(u"\u201c", "\"").replace(u"\u00c9", "E").replace(u"\u201d", "\"") + '</a><br>\n')
f.close()
