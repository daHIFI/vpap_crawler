from peewee import *

#update with your mySQL DB connection parameter
# passwd = 'password'
db = MySQLDatabase('test', host='localhost', user='root')

class BaseModel(Model):
    class Meta:
        database = db

class Dates(BaseModel):
    date = DateField()
    #processed = BooleanField()

class News(BaseModel):
    date = DateField()
    id = IntegerField(unique = True)
    cat = FixedCharField()
    title = CharField()
    url = CharField()
    body = TextField()
