from bs4 import BeautifulSoup
import requests
import re
import datetime
from peewee import *
from database import *

domain = 'http://vpap.org'


#get valid dates array from past news page
pastPage = requests.get('http://www.vpap.org/vanews/past')
bsPast = BeautifulSoup(pastPage.content)
datescript = bsPast.find_all("script")[5]
validDates = re.findall("([0-9]{1,2}\-[0-9]{1,2}\-[0-9]{4})", datescript.string)


#convert from mm-dd-yyyy to yyyy-mm-dd format
for date in validDates:
    print date
    date = datetime.datetime.strptime(date, '%m-%d-%Y').strftime('%Y-%m-%d')

    if Dates.select().where(Dates.date == date).exists():
        print date + " previously processed"
    else:
        url = 'http://www.vpap.org/vanews/past/?edition__date=' + date
        print 'processing ' + url
        page = requests.get(url);
        bs = BeautifulSoup(page.content)

        #get the meat of the page
        newsStories = bs.find_all("h4", class_="list-group-item-heading")

        db.connect()
        for story in newsStories:
            story.id = str(story.a)[30:35]
            print story.id;
            if not News.select().where(News.id == story.id).exists():
                link = domain + story.a['href']
                try:
                    linkPage = requests.get(link)# , verify=False\
                    story.url = linkPage.url
                except:
                    story.url = link


                story.cat = story.find_previous_sibling("p", class_="header").string.lstrip()
                story.title = story.a.string.strip()

                body = story.find_next_sibling().find_next_sibling("p").text
                story.body = " ".join(body.split())

                thisStory = News.create(date = date, id = story.id, cat = story.cat, url = story.url, title = story.title, body = story.body)
                thisStory.save()
        Dates.create(date = date).save
        print "done processsing " + date

        db.close()
