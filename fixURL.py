from bs4 import BeautifulSoup
import requests
import re
import datetime
from peewee import *
from database import *

domain = 'http://vpap.org'


#get valid dates array from past news page
pastPage = requests.get('http://www.vpap.org/vanews/past')
bsPast = BeautifulSoup(pastPage.content)
datescript = bsPast.find_all("script")[5]
validDates = re.findall("([0-9]{1,2}\-[0-9]{1,2}\-[0-9]{4})", datescript.string)


#convert from mm-dd-yyyy to yyyy-mm-dd format
for date in validDates:
    print date
    date = datetime.datetime.strptime(date, '%m-%d-%Y').strftime('%Y-%m-%d')
    if date > '2016-11-24':
        VPAPurl = 'http://www.vpap.org/vanews/past/?edition__date=' + date
        print 'processing ' + VPAPurl
        page = requests.get(VPAPurl);
        bs = BeautifulSoup(page.content)

        #get the meat of the page
        newsStories = bs.find_all("h4", class_="list-group-item-heading")

        db.connect()
        for story in newsStories:
            story.id = str(story.a)[30:35]

            link = domain + story.a['href']
            linkPage = requests.get(link)
            story.url = linkPage.url
            thisStory = News.update(url = story.url).where(News.id == story.id)
            News.select().where(story.id == id)
            thisStory.execute()
        print "done processsing " + date
        #Dates.create(date = date).save

        db.close()
