import datetime
from peewee import *
from database import *
from wordcloud import WordCloud, STOPWORDS

stopwords = set(STOPWORDS)
words = "virginia", "state", "will", "year", "new", "said", "county", "city", "school", "district"
for word in words:
    stopwords.add(word)

def makeCloud(range):
    text = ""
    query = News.raw('select title, body from news where date > ' + range)
    for news in query:
        text += news.title + " " + news.body

    wc = WordCloud(stopwords=stopwords)
    wc.generate(text)

    import matplotlib.pyplot as plt
    plt.imshow(wc)
    plt.axis("off")
    plt.show()


today = datetime.date.today()
#lastmonth

month = datetime.timedelta(days=-28)
lastmonth = today + month
monthdate = lastmonth.strftime("%Y-%m-%d")
makeCloud(monthdate)




#last week
week = datetime.timedelta(days=-7)
lastweek = today + week
weekdate = lastweek.strftime("%Y-%m-%d")
makeCloud(weekdate)
